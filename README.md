## Cadastro de veiculos

Implementação: Java 8, Spring Boot e Sprint Security.
Testes automáticos: [mockito](https://site.mockito.org)
Autenticação: [OAuth 2.0](https://oauth.net/2/). Dessa forma a API fica pronta para ser usara por diferentes tipos de clientes.

Para compilar o projeto:
**mvn clean install -DskipTests**

Arquivo para configuração do bando e usuário da API:
**src/main/resources/application.properties**

Arquivos com os scripts para criação das tabelas e dados:
**src/main/resources/script.sql**
**src/main/resources/tabelas.sql**

Com o banco de dados configurado: (para rodar os testes)
**mvn clean install**

Para subir a aplicação:
**mvn spring-boot:run**

Testes da API via [postman](https://www.getpostman.com)

Arquivo para importar no postman:
**postman/Cadastro-de-veiculos.postman_collection.json**

Executar a URL para gerar o token para usuário com permissão:
http://localhost:8080/oauth/token

Depois da geração do token (primeira linha do response da URL acima) passar o mesmo nas chamadas para as outras APIS.
Para isso selecione a aba Authorization e a opção OAuth 2.0 e copie o token no campo Access Token.
