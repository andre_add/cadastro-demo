package com.cadastro.cadastrodemo;

import com.cadastro.cadastrodemo.model.Marca;
import com.cadastro.cadastrodemo.model.Usuario;
import com.cadastro.cadastrodemo.model.Veiculo;
import com.cadastro.cadastrodemo.repository.MarcaRepository;
import com.cadastro.cadastrodemo.repository.RoleRepository;
import com.cadastro.cadastrodemo.repository.UsuarioRepository;
import com.cadastro.cadastrodemo.repository.VeiculoRepository;
import com.cadastro.cadastrodemo.service.UsuarioService;
import com.cadastro.cadastrodemo.service.VeiculoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CadastroDemoApplicationTests {

    private static final String MODELO = "Fit";
    private static final String PLACA = "EJB3030";

    @Mock
    private UsuarioRepository mockUsuarioRepository;

    @Mock
    private RoleRepository mockRoleRepository;

    @Mock
    private MarcaRepository mockMarcaRepository;

    @Mock
    private VeiculoRepository mockVeiculoRepository;

    private UsuarioService usuarioServiceTest;

    @Mock
    private VeiculoService veiculoServiceTest;

    @Before
    public void setUp() {
        initMocks(this);
        usuarioServiceTest = new UsuarioService(mockUsuarioRepository,
                mockRoleRepository);
        Usuario usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Andre");
        usuario.setEmail("andre@teste.com.br");

        Mockito.when(mockUsuarioRepository.save(any()))
                .thenReturn(usuario);
        Mockito.when(mockUsuarioRepository.findByEmail(anyString()))
                .thenReturn(usuario);

        Marca marca = new Marca();
        marca.setId(1L);
        marca.setNome("Honda");
        mockMarcaRepository.save(marca);

        Mockito.when(mockMarcaRepository.save(any())).thenReturn(marca);

        Veiculo veiculo = new Veiculo();
        veiculo.setMarca(marca);
        veiculo.setModelo(MODELO);
        veiculo.setAnoModelo(2018);
        veiculo.setAnoFabricacao(2018);
        veiculo.setPlaca(PLACA);
        veiculo.setCor("Preto");
        mockVeiculoRepository.save(veiculo);

        Mockito.when(mockVeiculoRepository.save(any())).thenReturn(veiculo);

        List<Veiculo> veiculos = new ArrayList<>();
        veiculos.add(veiculo);

        Mockito.when(mockVeiculoRepository.findAll()).thenReturn(veiculos);

        Mockito.when(veiculoServiceTest.buscarVeiculos(veiculo.getModelo(), marca.getId())).thenReturn(veiculos);
    }

    @Test
    public void testBuscarUsuarioPorEmail() {
        final String email = "andre@teste.com.br";
        final Usuario usuario = usuarioServiceTest.findUserByEmail(email);
        assertEquals(email, usuario.getEmail());
    }

    @Test
    public void testSalvarVeiculo() {

        final Marca marca = new Marca();
        marca.setNome("Honda");
        mockMarcaRepository.save(marca);

        final Veiculo veiculo = new Veiculo();
        veiculo.setMarca(marca);
        veiculo.setModelo(MODELO);
        veiculo.setAnoModelo(2018);
        veiculo.setAnoFabricacao(2018);
        veiculo.setPlaca(PLACA);
        veiculo.setCor("Preto");
        Veiculo veiculoSalvo = mockVeiculoRepository.save(veiculo);
        assertEquals(MODELO, veiculoSalvo.getModelo());
        assertEquals(PLACA, veiculoSalvo.getPlaca());
        assertEquals("Preto", veiculoSalvo.getCor());
    }

    @Test
    public void testBuscarVeiculos() {
        List<Veiculo> veiculos = mockVeiculoRepository.findAll();
        assertEquals(1, veiculos.size());
    }

    @Test
    public void testBuscarVeiculosPorFiltro() {
        List<Veiculo> veiculos = veiculoServiceTest.buscarVeiculos(MODELO, 1L);
        assertEquals(1, veiculos.size());
    }

    @Test
    public void testBuscarVeiculosPorFiltroInvalido() {
        List<Veiculo> veiculos = veiculoServiceTest.buscarVeiculos("Gol", 1L);
        assertEquals(0, veiculos.size());
    }


}

