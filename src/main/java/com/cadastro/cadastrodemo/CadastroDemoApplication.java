package com.cadastro.cadastrodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroDemoApplication.class, args);
	}

}

