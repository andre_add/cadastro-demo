package com.cadastro.cadastrodemo.service;

import com.cadastro.cadastrodemo.model.Marca;
import com.cadastro.cadastrodemo.model.Veiculo;
import com.cadastro.cadastrodemo.repository.MarcaRepository;
import com.cadastro.cadastrodemo.repository.VeiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class VeiculoService {

    @Autowired
    private VeiculoRepository veiculoRepository;

    @Autowired
    private MarcaRepository marcaRepository;

    public List<Veiculo> buscarVeiculos(String modelo, Long marcaId) {
        Veiculo veiculo = new Veiculo();
        if(marcaId != null && modelo != null) {
            return veiculoRepository.buscarVeiculosPorModeloEMarca(modelo, marcaId);
        } else if(modelo != null) {
            veiculo.setModelo(modelo);
            return veiculoRepository.findAll(Example.of(veiculo));
        } else if (marcaId != null) {
            Optional<Marca> marca = marcaRepository.findById(marcaId);
            if(marca.isPresent()) {
                veiculo.setMarca(marca.get());
                return veiculoRepository.findAll(Example.of(veiculo));
            } else {
                return Collections.emptyList();
            }
        }
        return veiculoRepository.findAll();
    }
}
