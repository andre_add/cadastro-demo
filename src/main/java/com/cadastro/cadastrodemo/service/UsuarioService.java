package com.cadastro.cadastrodemo.service;

import com.cadastro.cadastrodemo.model.Usuario;
import com.cadastro.cadastrodemo.repository.RoleRepository;
import com.cadastro.cadastrodemo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private RoleRepository roleRepository;

    public UsuarioService(UsuarioRepository usuarioRepository, RoleRepository roleRepository) {
        this.usuarioRepository = usuarioRepository;
        this.roleRepository = roleRepository;
    }

    public Usuario findUserByEmail(String email) {
        return usuarioRepository.findByEmail(email);
    }


}
