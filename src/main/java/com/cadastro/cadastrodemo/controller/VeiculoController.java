package com.cadastro.cadastrodemo.controller;

import com.cadastro.cadastrodemo.exception.RecursoNaoEncontradoException;
import com.cadastro.cadastrodemo.model.Marca;
import com.cadastro.cadastrodemo.model.Veiculo;
import com.cadastro.cadastrodemo.repository.MarcaRepository;
import com.cadastro.cadastrodemo.repository.VeiculoRepository;
import com.cadastro.cadastrodemo.service.VeiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
public class VeiculoController {

    @Autowired
    private VeiculoRepository veiculoRepository;

    @Autowired
    private MarcaRepository marcaRepository;

    @Autowired
    private VeiculoService veiculoService;

    @GetMapping("/veiculos")
    public List<Veiculo> consultarVeiculosPorModeloEMarca(
            @RequestParam(value = "modelo", required = false) String modelo,
            @RequestParam(value = "marca", required = false) Long marcaId) {
        return veiculoService.buscarVeiculos(modelo, marcaId);
    }

    @PostMapping("/veiculos")
    public Veiculo cadastrarVeiculo(@RequestBody Veiculo veiculo) {
        return veiculoRepository.save(veiculo);
    }

    @PutMapping("/veiculos/{veiculoId}")
    public Veiculo updateVeiculo(@PathVariable Long veiculoId, @RequestBody Veiculo veiculoRequest) {
        return veiculoRepository.findById(veiculoId)
                .map(veiculo -> {
                    veiculo.setCor(veiculoRequest.getCor());
                    veiculo.setAnoFabricacao(veiculoRequest.getAnoFabricacao());
                    veiculo.setAnoModelo(veiculoRequest.getAnoModelo());
                    veiculo.setModelo(veiculoRequest.getModelo());
                    veiculo.setPlaca(veiculoRequest.getPlaca());
                    return veiculoRepository.save(veiculo);
                }).orElseThrow(() -> new RecursoNaoEncontradoException("Veiculo nao encontrado com id: " + veiculoId));
    }

    @DeleteMapping("/veiculos/{veiculoId}")
    public ResponseEntity<?> deleteVeiculo(@PathVariable Long veiculoId) {
        return veiculoRepository.findById(veiculoId)
                .map(veiculo -> {
                    veiculoRepository.delete(veiculo);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new RecursoNaoEncontradoException("Veiculo nao encontrado com id: " + veiculoId));
    }
}
