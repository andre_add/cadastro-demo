package com.cadastro.cadastrodemo.repository;

import com.cadastro.cadastrodemo.model.Veiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {

    @Query("select v from Veiculo v where 1 =1 and v.modelo = ?1 and v.marca.id = ?2")
    List<Veiculo> buscarVeiculosPorModeloEMarca(String modelo, Long marcaId);



}
