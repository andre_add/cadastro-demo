-- Table: public.veiculo

-- DROP TABLE public.veiculo;

CREATE TABLE public.veiculo
(
    id bigint NOT NULL,
    ano_fabricacao integer,
    ano_modelo integer,
    cor character varying(255) COLLATE "default".pg_catalog,
    modelo character varying(255) COLLATE "default".pg_catalog,
    placa character varying(255) COLLATE "default".pg_catalog,
    marca_id bigint,
    CONSTRAINT veiculo_pkey PRIMARY KEY (id),
    CONSTRAINT fk9g5yovckuf4squ6rf8vpxu809 FOREIGN KEY (marca_id)
        REFERENCES public.marca (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.veiculo
    OWNER to postgres;

-- Table: public.marca

-- DROP TABLE public.marca;

CREATE TABLE public.marca
(
    id bigint NOT NULL,
    nome character varying(255) COLLATE "default".pg_catalog,
    CONSTRAINT marca_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.marca
    OWNER to postgres;

-- Table: public.usuario

-- DROP TABLE public.usuario;

CREATE TABLE public.usuario
(
    usuario_id integer NOT NULL,
    ativo integer,
    email character varying(255) COLLATE "default".pg_catalog,
    nome character varying(255) COLLATE "default".pg_catalog,
    senha character varying(255) COLLATE "default".pg_catalog,
    CONSTRAINT usuario_pkey PRIMARY KEY (usuario_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.usuario
    OWNER to postgres;

-- Table: public.usuario_role

-- DROP TABLE public.usuario_role;

CREATE TABLE public.usuario_role
(
    usuario_id integer NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT usuario_role_pkey PRIMARY KEY (role_id, usuario_id),
    CONSTRAINT fke7gfguqsiox6p89xggm8g2twf FOREIGN KEY (role_id)
        REFERENCES public.role (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkpc2qjts6sqq4hja9f6i3hf0ep FOREIGN KEY (usuario_id)
        REFERENCES public.usuario (usuario_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.usuario_role
    OWNER to postgres;

-- Table: public.usuario_role

-- DROP TABLE public.usuario_role;

CREATE TABLE public.usuario_role
(
    usuario_id integer NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT usuario_role_pkey PRIMARY KEY (role_id, usuario_id),
    CONSTRAINT fke7gfguqsiox6p89xggm8g2twf FOREIGN KEY (role_id)
        REFERENCES public.role (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkpc2qjts6sqq4hja9f6i3hf0ep FOREIGN KEY (usuario_id)
        REFERENCES public.usuario (usuario_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.usuario_role
    OWNER to postgres;
