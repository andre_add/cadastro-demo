-- marcas
insert into marca (id,nome) values (1,'Honda');
insert into marca (id,nome) values (2,'Fiat');

-- usuario que pode acessar a aplicacao via token
insert into usuario values (1,1,'usuario@teste','nome usuario','1234');
insert into usuario_role values (1,1);
insert into role values (1,'ADMIN');
